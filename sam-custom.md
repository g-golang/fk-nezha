# 

## Login

```bash
# callback's login
http://localhost:8085/oauth2/callback?code=2a11a7f270678d946ef3a4218ed1e890089c037da8e2c8ba5e43193bdebff0cb&state=jeaUfNegafXrmIcz

# TODO: 有一次取得用户名`huapox`及icon
```

## Anay

**agent**

函数式，集中在agent/main.go

- main[flag解析]> run
  - reportState_10minInfo
  - forLoop: grpcConnClient, RequestTask取列表> receiveTasksLoop依次接收> 
    - doTask[term,http,tcp,icmp,cmd,upgrade]> ReportTask[result]

**dashServer**

- rpc.ServeRPC(p1)
  - DispatchTask, kalive
- sentinel服务哨兵
  - AlertSentinelStart
  - ServiceSentinel 加载History记录；ServiceSentinelShared.worker()
- 
- controller.ServeWeb(p2)
  - common/guest/memberPage, 
  - /api, /api/v1(/server/list,details)
- **多主机控制**
  - singleton: db> ServerList[增改删] > SortedServerList[refresh]
  - 面板增改删主机(db)

## TODO

step0: 

- 写死管理用户
- shell调API注册节点
- 主页链接到VNC(URL ip:port)

### Login,自注册

Login写死，自注册用现有db先初始(shell调api);

### rpc> rest-chisel-tunnel

step1: grpc不动，新加http双向交互; step2: ser> agent改用chisel模式;

**221021**

- ~~pb代码引用情况~~
- ~~dash/agent启动流程~~, ~~dash多实例的管理(dbLoad)~~
- 
- **agent改rest模式**，http服务查看
- 新加chisel，agent连到server端(http> http)
- chisel版多实例的管理


